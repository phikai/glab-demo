# Recording Content

We use [terminalizer](https://github.com/faressoft/terminalizer) to record animated gifs for the GitLab CLI.

Configuration for terminalizer is in `/configuration/config.yml`

Recording is done in a 100x20 terminal window

## Optimizing

- https://gifcompressor.com/
- https://ezgif.com/optimize

Try removing frames, color pallete, optimize, etc..., to get the size small enough.
